		var demo = new Vue({
		  el: '#demo',
		  data: {
		    user: 'Hello Vue.js!',
		    list_user:[]
		  },
		  methods:{
		  	pushUser:function(){
		  		var self = this;
		  		var found = false;
		  		for(var i=0;i<self.list_user.length;i++){
		  			if(self.user==self.list_user[i]){
		  				found = true;
		  			}
		  		}
		  		if(found){
		  			console.dir("already exits")
		  		}else {
		  			if(this.user!=''){
			  			$.ajax({
			  				url:'/',
			  				type:'POST',
			  				data:{user:self.user},
			  				success:function(Item, Status){
			  					if(Item.status=='OK'){
			  						self.list_user.push(self.user);
			  						self.user = '';
			  					}
			  				}
			  			});
			  		}
		  		}
		  	},
		  	listUser:function(){
		  		var self = this;
		  		$.ajax({
		  				url:'/list',
		  				type:'GET',
		  				success:function(Item, Status){
		  					if(Item.status=='OK'){
		  						console.dir(Item.msg);
		  						self.list_user = self.list_user.concat(Item.msg);
		  					}
		  				}
		  			});
		  	},
		  	showUser:function(user){
		  		var self = this;
		  		$.ajax({
		  			url:'/del/'+user,
		  			type:'GET',
		  			success:function(Item, Status){
		  				if(Item.status=="OK"){
		  					for(var i=0;i<self.list_user.length;i++){
					  			if(user==self.list_user[i]){
					  				self.list_user.splice(i, 1);
					  			}
					  		}
		  				}
		  			}
		  		});
		  	}
		  }
		});

		demo.listUser();