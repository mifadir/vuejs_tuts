var express = require('express');
var app = express();

var bodyParser = require('body-parser');

app.use(bodyParser.urlencoded({ extended: false }));

app.use(express.static(__dirname+'/public'));

var list_user = [];

app.get('/', function(req, res){
	res.sendFile(__dirname+'/templates/index.html');
});

app.post('/', function(req, res){
	var user = req.body.user;
	console.dir(user);
	if(req.body.user!=undefined){
		list_user.push(req.body.user);
		var msg = {
			status:"OK",
			msg:"done"
		}
		res.send(msg);
	}else {
		var msg = {
			status:"ERR",
			msg:"done"
		}
		res.send(msg);
	}
});

app.get('/list', function(req, res){
	var msg = {
		status:"OK",
		msg:list_user
	};
	res.send(msg);
});

app.get('/del/:user', function(req, res){
	if(req.params.user!=undefined){
		for(var i=0;i<list_user.length;i++){
			if(req.params.user==list_user[i]){
				list_user.splice(i, 1);
				var msg = {
					status:"OK",
					msg:"deleted"
				};
				res.send(msg);
			}
		}
	}else {
		var msg = {
			status:"ERR",
			msg:"invalid user"
		};
		res.send(msg);
	}
});

app.listen(8080, '0.0.0.0');
console.log("http://localhost:8080");